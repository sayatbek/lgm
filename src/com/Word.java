package com;

import java.util.Comparator;
import java.util.HashSet;

public class Word implements Comparable<Word>{
	public String name;
	public int count;
	public boolean existLGM;
	public boolean existUch;
	
	public Word(String n){
		name= n ;
		count = 0;
		existLGM = false;
		existUch = false;
	}
	
	public Word(String n, int c){
		name= n ;
		count = c;
		existLGM = false;
		existUch = false;
	}
	
	@Override
	public String toString(){
		return name+"\t"+count;
	}
	
	@Override 
	public int hashCode() {
	    /*int result=0;
	    int n = this.name.length();
	    for(int i=0;i<n;i++){
	    	result += this.name.charAt(i)*31^(n - (i+1));
	    }
	    return result;*/
		return this.name.hashCode();
	}
	
	@Override
	public int compareTo(Word arg0) {
			return this.name.compareTo(arg0.name);
	}
	
	@Override
	public boolean equals(Object arg0){
		Word w = (Word) arg0;
		return this.name.equals(w.name); 
	}
	
	public boolean isSimilar(Word w, DBConnection db){
		if(w.name.length() < this.name.length()){
			return false;
		}
		if(!w.name.contains(this.name)){
			return false;
		}
		
		Stemmer stemmer = new Stemmer(db);
		String ww = stemmer.stemWord(w.name);
		if(ww != null){
			return this.name.equals(ww);
		}
		else{
			for(int i=0;i<this.name.length(); i++){
				if(this.name.charAt(i) != w.name.charAt(i)){
					return false;
				}
			}
			return true;
		}
	}
	
	public boolean isSimilar(Word w){
		if(w.name.length() < this.name.length()){
			return false;
		}
		if(!w.name.contains(this.name)){
			return false;
		}
		for(int i=0;i<this.name.length(); i++){
			if(this.name.charAt(i) != w.name.charAt(i)){
				return false;
			}
		}
		return true;
	}
	
	public Word trimZh(){
		HashSet<String> zhalgau = new HashSet<String>();
		zhalgau.add("дар");zhalgau.add("дер");
		zhalgau.add("лар");zhalgau.add("лер");
		zhalgau.add("тар");zhalgau.add("тер");

		HashSet<Character> bukvy = new HashSet<Character>();
		bukvy.add('л');bukvy.add('м');bukvy.add('н');bukvy.add('ң');bukvy.add('ж');bukvy.add('з');
		
		HashSet<Character> katan = new HashSet<Character>();
		katan.add('п');katan.add('к');katan.add('қ');katan.add('т');katan.add('с');katan.add('ф');
		katan.add('ф');katan.add('х');katan.add('ц');katan.add('ч');katan.add('ш');katan.add('б');
		katan.add('в');katan.add('г');katan.add('д');		
		
		HashSet<Character> duisti = new HashSet<Character>();
		duisti.add('а');duisti.add('о');duisti.add('е');duisti.add('ы');duisti.add('і');duisti.add('о');
		duisti.add('ө');duisti.add('ұ');duisti.add('ү');duisti.add('й');duisti.add('р');duisti.add('у');
		
		String w = this.name;
		if(w.length() >= 5){
	    	String zh = w.substring(w.length()-3, w.length());
	    	if(zhalgau.contains(zh)){
	    		if(bukvy.contains(w.charAt(w.length()-4))&&(zh.equals("дар") || zh.equals("дер"))){
	    			this.name = (w.substring(0, w.length()-3));
	    		}
	    		else if(katan.contains(w.charAt(w.length()-4))&&(zh.equals("тар") || zh.equals("тер"))){
	    			this.name = (w.substring(0, w.length()-3));
		    	}
	    		else if(duisti.contains(w.charAt(w.length()-4))&&(zh.equals("лар") || zh.equals("лер"))){
	    			this.name = (w.substring(0, w.length()-3));
		    	}
	    	}
		}	 
		
		
		return this;
	 }

}
