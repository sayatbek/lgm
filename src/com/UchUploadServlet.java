package com;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.pdfbox.pdmodel.PDDocument;

@WebServlet(name = "UchUploadServlet", urlPatterns = {"/uploadUch"})
@MultipartConfig

public class UchUploadServlet extends HttpServlet {

    private final static Logger LOGGER = 
            Logger.getLogger(FileUploadServlet.class.getCanonicalName());
   
	public static TreeMap<String, Integer> words;
	public static Connection conn = (new DBConnection()).getConnection();
	
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        words = new TreeMap<String, Integer>();
        // Create path components to save the file
        final String path = "/usr/soz/"+request.getParameter("destinationMin");
        final String grade = request.getParameter("grade");
        final Part filePart = request.getPart("fileMin");
        final String fileName = getFileName(filePart);

        OutputStream out = null;
        InputStream filecontent = null;
        final PrintWriter writer = response.getWriter();
        
        Stack<Character> s = new Stack<Character>();
        for(int i=fileName.length()-1;i>=0;i--){
        	if(fileName.charAt(i)=='.'){break;}
        	s.push(fileName.charAt(i));
        }
        
        String extension = "";
        while(!s.empty()){
        	extension += (s.pop()).toString();
        }
        
        try {
            out = new FileOutputStream(new File(path + File.separator
                    + fileName));
            
            filecontent = filePart.getInputStream();

            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            writer.println("New file " + fileName + " created at " + path);
            LOGGER.log(Level.INFO, "File{0}being uploaded to {1}", 
                    new Object[]{fileName, path});
            
            request.setCharacterEncoding("UTF-8");
    		request.getRequestDispatcher("pushUchToDB.jsp?grade="+grade+"&uploaded="+fileName+"&path="+path).forward(request, response);
    		
        } catch (FileNotFoundException fne) {
            writer.println("You either did not specify a file to upload or are "
                    + "trying to upload a file to a protected or nonexistent "
                    + "location.");
            writer.println("<br/> ERROR: " + fne.getMessage());

            LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}", 
                    new Object[]{fne.getMessage()});
    		
        } catch(NullPointerException e){
        	System.out.println(e.getStackTrace());
        }
        
       
    }

    public static void putTextToHashSet(String grade, String st){
		String[] arr = st.toLowerCase().split("\\s+");
     	for(int i=0;i<arr.length;i++){
     		arr[i] = stripWord(arr[i]);
		   	if(arr[i].length()>1){
				String w = (arr[i].trim().toLowerCase());
		   		putTextToDataBase(grade, w);
		   	}
	    }
     	arr = null;
    }
    
    public static void putTextToDataBase(String grade, String word){
    		try{
    			
    	    	String sql = "INSERT INTO uchebnik (`word`, `grade`) VALUES (?, ?)";
    	    	PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    			stmt.setString(1, word);
    			stmt.setString(2, grade);
        		stmt.execute();
        	}catch(Exception e){
    			System.out.println(e.getMessage());
    		}
    }
    
    public static String stripWord(String word){
    	word = word.replaceAll("i", "і");
 		word = word.replaceAll("ї", "і");
 		word = word.replaceAll("a", "а");
 		word = word.replaceAll("b", "б");
 		word = word.replaceAll("є", "қ");
 		word = word.replaceAll("ііі", "ш");
 		word = word.replaceAll("¥", "ұ");
 		word = word.replaceAll("ӛ", "ө");
 		word = word.replaceAll("ё", "е");
 		
 		word = word.replaceAll("(?!-)\\p{Punct}|\\p{IsDigit}|\\^-|x|v|l|«|»||ї|\\s|…|©|№|[a-zA-Z]|—|¹|”|“|•", "");
    	try{
	 		for(int j=0;j<5;j++){
		   		if(word.length()>1){
		   			if(word.charAt(word.length()-1) == '-' || word.charAt(0) == '–'){word = word.substring(0, word.length()-1);}
		   			if(word.charAt(0) == '-' || word.charAt(0) == '–'){ word = word.substring(1, word.length());}
		   		}
	    	}
    	}catch(Exception e){}
    	
    	return word;
    }
    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}