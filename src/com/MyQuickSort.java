package com;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class MyQuickSort {
    
    private ArrayList<Word> array;
    private int length;
 
    public void sort(ArrayList<Word> inputArr) {
         
        if (inputArr == null || inputArr.size() == 0) {
            return;
        }
        this.array = inputArr;
        length = inputArr.size();
        quickSort(0, length - 1);
    }
 
    private void quickSort(int lowerIndex, int higherIndex) {
         
        int i = lowerIndex;
        int j = higherIndex;
        // calculate pivot number, I am taking pivot as middle index number
        int pivot = array.get(lowerIndex+(higherIndex-lowerIndex)/2).count;
        // Divide into two arrays
        while (i <= j) {
            /**
             * In each iteration, we will identify a number from left side which
             * is greater then the pivot value, and also we will identify a number
             * from right side which is less then the pivot value. Once the search
             * is done, then we exchange both numbers.
             */
            while (array.get(i).count > pivot) {
                i++;
            }
            while (array.get(j).count < pivot) {
                j--;
            }
            if (i <= j) {
            	 Collections.swap(array, i, j);
                //move index to next position on both sides
                i++;
                j--;
            }
            
        }
        // call quickSort() method recursively
        if (lowerIndex < j)
            quickSort(lowerIndex, j);
        if (i < higherIndex)
            quickSort(i, higherIndex);
    }
}
