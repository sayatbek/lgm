package com;

import java.awt.Font;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;

@WebServlet(name = "ExcelServlet", urlPatterns = {"/excel"})
@MultipartConfig

public class ExcelServlet extends HttpServlet{
	 protected void doPost(HttpServletRequest request,
	            HttpServletResponse response)
	            throws ServletException, IOException {
	     response.setContentType("text/html;charset=UTF-8");
	     
	     
	     try{
	     final Connection conn = (new DBConnection()).getConnection();
	     PreparedStatement stmt= null, stmt1 = null;
	     ResultSet rs = null, rs1 = null;
	     String sql = "";

	     String gLgm = request.getParameter("gLgm");
	     String gUch = request.getParameter("gUch");

	     ArrayList<Word> lgm = new ArrayList<Word>();
	     ArrayList<Word> words = new ArrayList<Word>();
	     
	     sql = "select word from lgm where grade=? order by `word` ASC";
	     stmt = conn.prepareStatement(sql);
	     stmt.setString(1, gLgm);
	     rs = stmt.executeQuery();
	     while(rs.next()){
	     	lgm.add(new Word(rs.getString(1)));
	     }

	     sql = "select word from uchebnik where grade=? order by `word` ASC";
	     stmt = conn.prepareStatement(sql);
	     stmt.setString(1, gUch);
	     rs = stmt.executeQuery();
	     while(rs.next()){
	     	words.add(new Word(rs.getString(1)));
	     }

	     
	     int allLgm = 0;
	     int allUch =0;
	     for(Word l : lgm){
	     	boolean bil = false;
	    	for(Word w : words){
	     		if(l.isSimilar(w)){
	     			l.count++;
	     			allUch++;
	     			if(!bil){
	     				allLgm++;
	     				bil = true;
	     			}
	     		}
	     	}
	     }
	    
	     
	     HSSFWorkbook doc = new HSSFWorkbook();
	     HSSFSheet sheet = doc.createSheet("ЛГМ за "+gLgm+" класс на Учебник "+gUch+" за 5 класс");
	     
	     CellStyle cs = doc.createCellStyle();
	     cs.setWrapText(true);
	     
	     int rowCounter=0;
	     int cols = 0;
	     HSSFRow row = sheet.createRow(rowCounter);
	     rowCounter++;
	     
	     HSSFCell cell0 = row.createCell(cols++);
	     cell0.setCellValue("Слова из ЛГМ  которые есть в учебнике");
	     
	     HSSFCell cell1 = row.createCell(cols++);
	     cell1.setCellValue("Частота повторения в учебнике");
	     
	     HSSFCell cell2 = row.createCell(cols++);
	     cell2.setCellValue("Слова из ЛГМ которых нет в учебнике");
	     
	     HSSFCell cell5 = row.createCell(cols++);
	     cell5.setCellValue("% слов из ЛГМ которые присутствуют Учебнике");
	     
	     HSSFCell cell6 = row.createCell(cols++);
	     cell6.setCellValue("% ВСЕХ слов Учебника взятых из ЛГМ (с учетом их повторения в Учебнике)");
	     
	     
	     HSSFFont font= doc.createFont();
	     font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	     
	     cs.setFont(font);
	     
	     cell0.setCellStyle(cs);
	     cell1.setCellStyle(cs);
	     cell2.setCellStyle(cs);
	     cell5.setCellStyle(cs);
	     cell6.setCellStyle(cs);
	     
	     int popal = 0;
	     int popal1 = 0;
	     for(Word l : lgm){
	     	if(l.count>0){
	     		HSSFRow r = sheet.createRow(rowCounter);
		     	r.createCell(0).setCellValue(l.name);
	     		r.createCell(1).setCellValue(l.count);
	     		rowCounter++;
	     		popal++;
	     		popal1 += l.count;
		    }
	     }
	     HSSFRow rw = sheet.createRow(rowCounter);
	     rw.createCell(0).setCellValue("Всего: "+popal);
	     rw.createCell(1).setCellValue(popal1);
	   
	     
	     rowCounter=1;
	     for(Word l : lgm){
	    	if(l.count== 0){
		    	HSSFRow r = sheet.getRow(rowCounter);
				if(r == null){
			     	r = sheet.createRow(rowCounter);
			    }
			    r.createCell(2).setCellValue(l.name);
			    rowCounter++;
			}
		 }
	     HSSFRow rw1 = sheet.getRow(rowCounter);
		 if(rw1 == null){
		   	rw1 = sheet.createRow(rowCounter);
		 }
    	 rw1.createCell(2).setCellValue("Всего: "+(rowCounter-1));
	     
    	 
  		 int pers = (popal*100)/lgm.size();
	     
	     HSSFRow rp1 = sheet.getRow(1);
	     if(rp1 == null){
			   	rp1 = sheet.createRow(1);
		 }
	     rp1.createCell(3).setCellValue(pers+"%");
	     
	     pers = allUch*100/words.size();
	     rp1.createCell(4).setCellValue(pers+"%");
	     
	     //write it as an excel attachment
	     ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
	     doc.write(outByteStream);
	     byte [] outArray = outByteStream.toByteArray();
	     response.setContentType("application/ms-excel");
	     response.setContentLength(outArray.length);
	     response.setHeader("Expires:", "0"); // eliminates browser caching
	     response.setHeader("Content-Disposition", "attachment; filename=testxls.xls");
	     OutputStream outStream = response.getOutputStream();
	     outStream.write(outArray);
	     outStream.flush();
	     }
	     catch(Exception e){
	    	 e.printStackTrace();
	     }
	 }
}
