package com;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

@WebServlet(name = "FileUploadServlet", urlPatterns = {"/upload"})
@MultipartConfig

public class FileUploadServlet extends HttpServlet {

    private final static Logger LOGGER = 
            Logger.getLogger(FileUploadServlet.class.getCanonicalName());
   
	
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        ArrayList<Word> words;
        ArrayList<Word> lgm;
    	TreeSet<Word> lgmHash;
    	TreeSet<Word> wordsHash;
        
        words = new ArrayList<Word>();
        lgm = new ArrayList<Word>();
        lgmHash = new TreeSet<Word>();
        wordsHash = new TreeSet<Word>();
        
        // Create path components to save the file
        final String path = "/usr/soz/"+request.getParameter("destination");
        final Part filePart = request.getPart("file");
        final String fileName = getFileName(filePart);
        final String extension = request.getParameter("extension");

        Stack<Character> s = new Stack<Character>();
        for(int i=fileName.length()-1;i>=0;i--){
        	if(fileName.charAt(i)=='.'){break;}
        	s.push(fileName.charAt(i));
        }
        
        String extensionUch = "";
        while(!s.empty()){
        	extensionUch += (s.pop()).toString();
        }
        
        OutputStream out = null;
        InputStream filecontent = null;
        final PrintWriter writer = response.getWriter();

        try {
            out = new FileOutputStream(new File(path + File.separator
                    + fileName));
            filecontent = filePart.getInputStream();

            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            writer.println("New file " + fileName + " created at " + path);
            LOGGER.log(Level.INFO, "File{0}being uploaded to {1}", 
                    new Object[]{fileName, path});
        } catch (FileNotFoundException fne) {
            writer.println("You either did not specify a file to upload or are "
                    + "trying to upload a file to a protected or nonexistent "
                    + "location.");
            writer.println("<br/> ERROR: " + fne.getMessage());

            LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}", 
                    new Object[]{fne.getMessage()});
        } catch(NullPointerException e){
        	System.out.println(e.getStackTrace());
        }
        
        /*if(extension.equals("docx")){
        	File file = null;
        	XWPFWordExtractor extractor = null;
            try
            {

                file = new File(fileName);
                FileInputStream fis = new FileInputStream(path + File.separator + "lgm."+extension);
                
                XWPFDocument document = new XWPFDocument(fis);

                extractor = new XWPFWordExtractor(document);
                
                putTextToHashSet(lgm, extractor.getText());
                
                document = null;
                extractor = null;
                file = null;
            }
            catch (Exception exep)
            {
                exep.printStackTrace();
            }
        }
        if(extensionUch.equals("docx")){
        	File file = null;
            WordExtractor extractor = null;
            try
            {

                file = new File(fileName);
                FileInputStream fis = new FileInputStream(path + File.separator + fileName);
                
                XWPFDocument document = new XWPFDocument(fis);
                extractor = new XWPFWordExtractor(document);
                
                putTextToHashSet(words, extractor.getText());
                
                document = null;
                extractor = null;
                file = null;
            }
            catch (Exception exep)
            {
                exep.printStackTrace();
            }
        }
        */
        if(extension.equals("pdf")){
	        try{
	    		PDDocument document = null; 
	    	    document = PDDocument.load(new File(path + File.separator + "lgm."+extension));
	    		document.getClass();
	    		if( !document.isEncrypted() ){
	    		     PDFTextStripper Tstripper = new PDFTextStripper();
	    		     
	    		     putTextToHashSet(lgm,Tstripper.getText(document));
	    		     
	    		     Tstripper = null;
	    		}
	    		document.close();
	    		document = null;
	    	}catch(Exception e){
	    	    e.printStackTrace();
	    	}	
        }
        
	    if(extensionUch.equals("pdf")){
	        try{
	    		PDDocument document = null; 
	    	    document = PDDocument.load(new File(path + File.separator + fileName));
	    		document.getClass();
	    		if( !document.isEncrypted() ){
	    		     PDFTextStripper Tstripper = new PDFTextStripper();
	    		     
	    		     putTextToHashSet(words, Tstripper.getText(document));
	    		     
	    		     Tstripper = null;
	    		}
	    		document.close();
	    		document = null;
	    	}catch(Exception e){
	    	    e.printStackTrace();
	    	}	
        }
        
       	
       	for(Word l : lgm){
       		for(Word w : words){
       			if(equals(l.name, w.name)){
       				l.count++;
       			}
       		}
       	}
       	
       	for(Word l : lgm){
       		lgmHash.add(l);
       	}
       	
       	for(Word l : words){
       		wordsHash.add(l);
       	}
       	
       	System.out.println(lgmHash.size());
       	System.out.println(wordsHash.size());
       	
       	request.setAttribute("lgmHash", lgmHash);
       	request.setAttribute("wordsHash", wordsHash);
       	request.setAttribute("lgm", lgm);
       	request.setAttribute("words", words);
        request.getRequestDispatcher("compare.jsp").forward(request, response);
    }

    public static boolean equals(String w1, String w2){
    	return w1.equalsIgnoreCase(w2);
    }
    
    public static void putTextToHashSet(ArrayList<Word> m,String st){
    	String[] arr = st.toLowerCase().split("\\s+");
     	for(int i=0;i<arr.length;i++){
     		arr[i] = stripWord(arr[i]);
		   	if(arr[i].length()>1){
				//hSet.add(arr[i].trim().toLowerCase());
		   		Word w = new Word(arr[i].trim().toLowerCase());	
		   		m.add(w);
		   	}
	    }
     	arr = null;
    }
    
    public static String stripWord(String word){
    	word = word.replaceAll("i", "і");
 		word = word.replaceAll("ї", "і");
 		word = word.replaceAll("a", "а");
 		word = word.replaceAll("b", "б");
 		word = word.replaceAll("є", "қ");
 		word = word.replaceAll("ііі", "ш");
 		word = word.replaceAll("¥", "ұ");
 		word = word.replaceAll("ӛ", "ө");
 		word = word.replaceAll("ё", "е");
 		
 		word = word.replaceAll("(?!-)\\p{Punct}|\\p{IsDigit}|\\^-|x|v|l|«|»||ї|\\s|…|©|№|[a-zA-Z]|—|¹|”|“|•", "");
    	try{
	 		for(int j=0;j<5;j++){
		   		if(word.length()>1){
		   			if(word.charAt(word.length()-1) == '-' || word.charAt(0) == '–'){word = word.substring(0, word.length()-1);}
		   			if(word.charAt(0) == '-' || word.charAt(0) == '–'){ word = word.substring(1, word.length());}
		   		}
	    	}
    	}catch(Exception e){}
    	
    	return word;
    }
    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}