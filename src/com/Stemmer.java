package com;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;

public class Stemmer {
	public static HashSet<String> terms;
	public static HashSet<String> zhalgau;
	public static HashSet<Character> bukvy;
	public static HashSet<Character> katan;
	public static HashSet<Character> duisti;
	
	public Stemmer(DBConnection db){
		this.terms = new HashSet<>();
		fillTerms(db);
		fillEndings();
	}
	public static void fillEndings(){
		zhalgau = new HashSet<String>();
		zhalgau.add("дар");zhalgau.add("дер");
		zhalgau.add("лар");zhalgau.add("лер");
		zhalgau.add("тар");zhalgau.add("тер");

		bukvy = new HashSet<Character>();
		bukvy.add('л');bukvy.add('м');bukvy.add('н');
		bukvy.add('ң');bukvy.add('ж');bukvy.add('з');
		
		katan = new HashSet<Character>();
		katan.add('п');katan.add('к');katan.add('қ');
		katan.add('т');katan.add('с');katan.add('ф');
		katan.add('ф');katan.add('х');katan.add('ц');
		katan.add('ч');katan.add('ш');katan.add('б');
		katan.add('в');katan.add('г');katan.add('д');		
		
		duisti = new HashSet<Character>();
		duisti.add('а');duisti.add('о');duisti.add('е');duisti.add('ы');duisti.add('і');duisti.add('о');
		duisti.add('ө');duisti.add('ұ');duisti.add('ү');duisti.add('й');duisti.add('р');duisti.add('у');
	}
	public static void fillTerms(DBConnection db){
		String sql = "select * from terms";
		try{
			Connection conn = db.getConnection();
			PreparedStatement stmt= null;
			stmt = conn.prepareStatement(sql);
		    
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				terms.add(rs.getString(2));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	public static boolean findInTerms(String s){
		if(terms.contains(s)){
			return true;
		}else{
			return false;
		}
	}
	public String trimKoptik(String w){
		if(w.length() >= 5){
	    	String zh = w.substring(w.length()-3, w.length());
	    	if(zhalgau.contains(zh)){
	    		if(bukvy.contains(w.charAt(w.length()-4))&&(zh.equals("дар") || zh.equals("дер"))){
	    			w = (w.substring(0, w.length()-3));
	    		}
	    		else if(katan.contains(w.charAt(w.length()-4))&&(zh.equals("тар") || zh.equals("тер"))){
	    			w = (w.substring(0, w.length()-3));
		    	}
	    		else if(duisti.contains(w.charAt(w.length()-4))&&(zh.equals("лар") || zh.equals("лер"))){
	    			w = (w.substring(0, w.length()-3));
		    	}
	    	}
		}	 
		return w;
	 }
	public String stemWord(String word){
		if(word.length()==0){
			return null;
		}
		if(findInTerms(word)){
			return word;
		}
		else{
			String w = trimKoptik(word);
			if(w.length()==word.length()){
				return stemWord(word.substring(0, word.length()-1));
			}
			else{
				return stemWord(w);
			}
		}
	}
}
