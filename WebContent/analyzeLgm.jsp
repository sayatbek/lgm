<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="com.MyQuickSort"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.TreeSet"%>
<%@page import="com.Word"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.catalina.tribes.group.interceptors.TwoPhaseCommitInterceptor.MapEntry"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.DBConnection"%>
<%@page import="java.sql.Connection"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="libs.jsp" />
<title>Анализ ЛГМ в учебнике</title>
</head>
<% 
	final Connection conn = (new DBConnection()).getConnection();
	PreparedStatement stmt= null, stmt1 = null;
	ResultSet rs = null, rs1 = null;
	String sql = "";
	
	String gLgm = request.getParameter("gLgm");
	String gUch = request.getParameter("gUch");
	
	//ArrayList<Word> lgm = new ArrayList<Word>();
	
	ArrayList<Word> lgmNew = new ArrayList<Word>();
	ArrayList<Word> wordsNew = new ArrayList<Word>();
	//TreeSet<Word> dict = new TreeSet<Word>();
	
	
	 sql = "select word from lgm where grade=? order by `word` ASC";
     stmt = conn.prepareStatement(sql);
     stmt.setString(1, gLgm);
     rs = stmt.executeQuery();
     while(rs.next()){
     	lgmNew.add(new Word(rs.getString(1)));
     }

     sql = "select word from uchebnik where grade=? order by `word` ASC";
     stmt = conn.prepareStatement(sql);
     stmt.setString(1, gUch);
     rs = stmt.executeQuery();
     while(rs.next()){
     	wordsNew.add(new Word(rs.getString(1)));
     }
     
     
     int allLgm = 0;
     int allUch = 0;
     for(Word l : lgmNew){
     	boolean bil = false;
    	for(Word w : wordsNew){
     		if(l.isSimilar(w)){
     			l.count++;
     			allUch++;
     			if(!bil){
     				allLgm++;
     				bil = true;
     			}
     		}
     	}
     }
    
%>
<body>
<jsp:include page="menus.jsp" />

<div class="container">
  	<div class="row">
	  <div class="col-sm-4">
		  <h2>Сравнение</h2>
		  <p>Слова из ЛГМ и их частота повторения в учебнике</p>
		  
		  	<form  class="form-group" method="POST" action="excel" enctype="multipart/form-data" >
		  		<input type="hidden" value="<%=gLgm%>" name="gLgm" /> 
		        <input type="hidden" value="<%=gUch %>" name="gUch"/>
		        <input class="btn btn-success" type="submit" value="Выгрузить в эксель" name="uploadMin" id="uploadMin" />
		    </form>
		</div>
		<div class="col-sm-6">
					 <%int pers = (allLgm*100)/(lgmNew.size());%>
					 	<table class="table">
					    <thead>
						  <tr>
					        <th><%=pers%>% слов из ЛГМ встретились Учебнике</th>
					      </tr>
					    </thead>
					    <tbody>
					      <tr><td class="center" align="center">
						    <div class="progress">
							  <div class="progress-bar" role="progressbar" aria-valuenow="<%=pers %>"
							  aria-valuemin="0" aria-valuemax="100" style="width:<%=pers%>%">
							    <%=pers%>% 
							  </div>
							</div>
						  </td></tr>
						</tbody>
					    </table>
			
					 <% pers = (allUch*100)/(wordsNew.size());%>
					 	<table class="table">
					    <thead>
						   <tr>
					        <th><%=pers%>% из ВСЕХ слов Учебника взято из ЛГМ (с учетом их повторения в Учебнике) </th>
					      </tr>
					    </thead>
					    <tbody>
					     <tr><td class="center" align="center">
						    <div class="progress">
							  <div class="progress-bar" role="progressbar" aria-valuenow="<%=pers %>"
							  aria-valuemin="0" aria-valuemax="100" style="width:<%=pers%>%">
							    <%=pers%>% 
							  </div>
							</div>
						  </td></tr>
						</tbody>
					    </table>
		</div>
	</div>
	<div class="row">
	 	<div class="col-sm-3"><h4>Сорт по алфавиту</h4></div>
	 	<div class="col-sm-2"><h4></h4></div>
	 	<div class="col-sm-3"><h4>Сорт по частоте</h4></div>
	 	<div class="col-sm-2"><h4></h4></div>
   	</div>
	<div class="row">
	 <div class="col-sm-3">
	  <table class="table">
	    <thead>
	      <tr>
	        <th>Слова из ЛГМ которые есть в учебнике</th>
	        <th>Частота повторения в учебнике</th>
	      </tr>
	    </thead>
	    <tbody>
	   	  <%
	   	  	allLgm = 0;
	   	    int allLgmTotal = 0; 
	   	 	 for(Word w : lgmNew){
	   	 		 if(w.count>0){
	   	 		 	allLgm++;
	   	 		 	allLgmTotal += w.count;
						      %>
						      <tr class="active">
						        <td><div class="cell bg-success"><%=w.name %></div></td>
						        <td><div class="cell bg-success"><%=w.count%></div>
						        </td>
						      </tr>
						
			    	<%}%>
		      <%} %>
		      <tr class="active">
		        <td><div class="cell bg-warning">Всего: <%=allLgm%></div></td>
		        <td><div class="cell bg-warning"><%=allLgmTotal%></div></td>
		      </tr>
	    </tbody>
	  </table>
	 </div>
	 
	 <div class="col-sm-2">
	  <table class="table">
	    <thead>
	      <tr>
	        <th>Слова из ЛГМ <br>которых нет<br> в учебнике</th>
	      </tr>
	    </thead>
	    <tbody>
	   		  <%
	   		  int allnLgm = 0;
	   		  for(Word w : lgmNew){
			      if(w.count == 0){%>
			      <tr class="active">
			        <td><div class="cell bg-danger"><%=w.name %></div></td>
			      </tr>
			      <% 
			      allnLgm++;
			      }
		      }%>
		      <tr class="active">
		        <td><div class="cell bg-warning">Всего: <%=allnLgm%></div></td>
		      </tr>
	   	</tbody>
	  </table>
	 </div>
	 
	 <div class="col-sm-3">
	  <table class="table">
	    <thead>
	      <tr>
	        <th>Слова из ЛГМ которые есть в учебнике</th>
	        <th>Частота повторения в учебнике</th>
	      </tr>
	    </thead>
	    <tbody>
	      <%
	   		MyQuickSort sorter = new MyQuickSort();
	        sorter.sort(lgmNew);
	   	  	for(Word w : lgmNew){
		      if(w.count > 0){%>
		      <tr class="active">
		        <td><div class="cell bg-success"><%=w.name %></div></td>
		        <td><div class="cell bg-success"><%=w.count %></div></td>
		      </tr>
		      <% 
		      }
		    }%>
		    <tr class="active">
		        <td><div class="cell bg-warning">Всего: <%=allLgm%></div></td>
		        <td><div class="cell bg-warning"><%=allLgmTotal%></div></td>
		      </tr>
	    </tbody>
	  </table>
	 </div>
	</div>
</div>
</body>
</html>