<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.DBConnection"%>
<%@page import="java.sql.Connection"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="libs.jsp" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script>
function po(){
	var gLgm = document.getElementById("selLgm");
	var gradeLgm = gLgm.options[gLgm.selectedIndex].value;
	
	var gUch = document.getElementById("selUch");
	var gradeUch = gUch.options[gUch.selectedIndex].value;
	
	document.location.href = "analyzeLgm.jsp?gLgm="+gradeLgm+"&gUch="+gradeUch;
}

var waitingDialog = waitingDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			//$dialog.modal('hide');
		}
	};

})(jQuery);

</script>
<title>Анализ</title>
</head>
<%
	final Connection conn = (new DBConnection()).getConnection();
	PreparedStatement stmt = null;
	ResultSet rs = null;
	String sql = "";
%>
<body>
<jsp:include page="menus.jsp" />

	<div class = "container">
		<div class="row">
			<div class="col-md-4">
				<label for="sel1">Выберите ЛГМ:</label>
			</div>
			<div class="col-md-4">
				<label for="sel1">Выберите Учебное Пособие:</label>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				  <select class="form-control" id="selLgm">
				  	<%
				  	 sql = "select grade, filename from files where type=1 order by `grade` ASC";
				  	 stmt = conn.prepareStatement(sql);
	    	    	 rs = stmt.executeQuery();
	    	    	 while(rs.next()){
	    	    		 %>
	    	    		 <option value="<%=rs.getString(1)%>"><p class="bg-danger"><%=rs.getString(1) %> класс</p> (<%=rs.getString(2) %>)</option>
	    	    		 <%
	    	    	 }
				  	%>
				  </select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				  <select class="form-control" id="selUch">
				  	<%
				  	 sql = "select grade, filename from files where type=2 order by `grade` ASC";
				  	 stmt = conn.prepareStatement(sql);
	    	    	 rs = stmt.executeQuery();
	    	    	 while(rs.next()){
	    	    		 %>
	    	    		 <option value="<%=rs.getString(1)%>"><p class="bg-danger"><%=rs.getString(1) %> класс</p> (<%=rs.getString(2) %>)</option>
	    	    		 <%
	    	    	 	 
	    	    	 }
				  	%>
				  </select>
				</div>
			</div>
			
			<div class="col-md-2">
				<a class="btn btn-large btn-primary" onclick="po(); waitingDialog.show('Идет анализ...');setTimeout(function () {waitingDialog.hide();}, 2000);" id="lgmuchanalyze">Готово</a>
			</div>
		</div>
	</div>
</body>
</html>