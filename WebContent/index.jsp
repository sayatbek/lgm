<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<jsp:include page="libs.jsp" />

<html lang="en">
  <head>
    <title>Лексико-грамматический минимум</title>
  </head>
  <body>

    <jsp:include page="menus.jsp" />

    <div class="container">
	   <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
      	<h3>Навигация на верхнем меню!</h3>
      	
      	<h4>Инструкции</h4>
      	<h5>Формат файлов только .pdf!</h5>
        <h5>1) Загрузите ЛГМ (Лексико Граммтический Минимум) для определенных классов, которые у Вас есть</h5>
        <h5>2) Загрузите Учебные пособия для определенных классов, которые у Вас есть</h5>
        <h5>3) Выберите пункт меню Анализ и выберите какой анализ необходим</h5>
        <h5>4) Выберите ЛГМ и учебники определенных классов для анализа</h5>
        <h6>5) Наслаждайтесь результатом </h6>
   
	 </div>
    </div> <!-- /container -->
   </body>
</html>
