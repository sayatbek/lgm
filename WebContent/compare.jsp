<%@page import="java.util.HashSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.Word"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Сравнение</title>
 <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/bootstrap-theme.min.css" rel="stylesheet">
</head>
<body>
	<!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.jsp">Главная</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    
	<%
	ArrayList<Word>  lgm = (ArrayList<Word>)request.getAttribute("lgm");
	ArrayList<Word>  words = (ArrayList<Word>)request.getAttribute("words");
	TreeSet<Word> lgmHash = (TreeSet<Word>)request.getAttribute("lgmHash");
	TreeSet<Word> wordsHash = (TreeSet<Word>)request.getAttribute("wordsHash");
	
	ArrayList<Word> lgmSortedByChast = new ArrayList<Word>();
	for(Word w : lgmHash){
		lgmSortedByChast.add(w);
	}
	
	for(Word l : lgmHash){
		for(Word w : wordsHash){
			if(l.name.equals(w.name)){
				l.existUch = true;
				break;
			}
		}	
	}
	for(Word w : wordsHash){
		for(Word l : lgmHash){
			if(l.name.equals(w.name)){
				w.existLGM = true;
				break;
			}
		}	
	}
	//out.println(lgm.size());
	//out.println(words.size());
	
	%>
<div class="container">
  <h2>Сравнение</h2>
  <p>Слова из ЛГМ и их частота повторения в учебнике</p>
  <div class="row">
 	<div class="col-sm-3"><h4>Сорт по алфавиту</h4></div>
 	<div class="col-sm-3"><h4>Сорт по частоте</h4></div>
 	<div class="col-sm-3"><h4>Слова из ЛГМ которых нет в учебнике</h4></div>
 	<div class="col-sm-3"><h4>Слова из Учебника которых нет в ЛГМ</h4></div>
   </div>
<div class="row">
 <div class="col-sm-3">
  <table class="table">
    <thead>
      <tr>
        <th>Слова из ЛГМ</th>
        <th>Частота повторения в учебнике</th>
      </tr>
    </thead>
    <tbody>
   	  <%
      //Collections.sort(lgmHash);
      for(Word w : lgmHash){
      if(w.existUch){
      %>
      <tr class="active">
        <td><%=w.name %></td>
        <td><%=w.count %></td>
      </tr>
      <%} 
      }%>
    </tbody>
  </table>
 </div>
 <div class="col-sm-3">
  <table class="table">
    <thead>
      <tr>
        <th>Слова из ЛГМ</th>
        <th>Частота повторения в учебнике</th>
      </tr>
    </thead>
    <tbody>
   <%
      for(int i=0;i<lgmSortedByChast.size();i++){
   		for(int j=0;j<lgmSortedByChast.size();j++){
   			if(lgmSortedByChast.get(i).count > lgmSortedByChast.get(j).count){
   				Collections.swap(lgmSortedByChast, i, j);
   			}
   		}
   	  }
      for(Word w : lgmSortedByChast){
      	if(w.existUch){%>
      <tr class="active">
        <td><%=w.name %></td>
        <td><%=w.count %></td>
      </tr>
      <%} 
      }%>
       </tbody>
  </table>
  </div>
 <div class="col-sm-3">
  <table class="table">
    <thead>
      <tr>
        <th>Слова из ЛГМ</th>
      </tr>
    </thead>
    <tbody>
   	  <%
      //Collections.sort(lgmHash);
      for(Word w : lgmHash){
    	  if(!w.existUch){
      %>
      <tr class="active">
        <td><%=w.name %></td>
      </tr>
      <%} 
      }%>
    </tbody>
  </table>
</div>
 <div class="col-sm-3">
  <table class="table">
    <thead>
      <tr>
        <th>Слова из ЛГМ</th>
      </tr>
    </thead>
    <tbody>
   	  <%
      //Collections.sort(lgmHash);
      for(Word w : wordsHash){
    	  if(!w.existLGM){
      %>
      <tr class="active">
        <td><%=w.name %></td>
      </tr>
      <%} 
      }%>
    </tbody>
  </table>
 </div>
</div>

</body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>
 
</html>