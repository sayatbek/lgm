<%@page import="java.io.OutputStream"%><%@page import="java.io.ByteArrayOutputStream"%><%@page import="java.util.HashSet"%><%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%><%@page import="com.sun.rowset.internal.Row"%><%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%><%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%><%@page import="java.sql.ResultSet"%><%@page import="java.sql.PreparedStatement"%><%@page import="com.DBConnection"%><%@page import="java.sql.Connection"%><%@page import="com.Word"%><%@page import="java.util.ArrayList"%><%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
final Connection conn = (new DBConnection()).getConnection();
PreparedStatement stmt= null, stmt1 = null;
ResultSet rs = null, rs1 = null;
String sql = "";

String gLgm = request.getParameter("gLgm");
String gUch = request.getParameter("gUch");

ArrayList<Word> lgm = new ArrayList<Word>();
ArrayList<Word> words = new ArrayList<Word>();
HashSet<Word> setW = new HashSet<Word>();

sql = "select word from lgm where grade=? order by `word` ASC";
stmt = conn.prepareStatement(sql);
stmt.setString(1, gLgm);
rs = stmt.executeQuery();
while(rs.next()){
	lgm.add(new Word(rs.getString(1)));
}

sql = "select word from uchebnik where grade=? order by `word` ASC";
stmt = conn.prepareStatement(sql);
stmt.setString(1, gLgm);
rs = stmt.executeQuery();
while(rs.next()){
	words.add(new Word(rs.getString(1)));
}

for(Word l : lgm){
	for(Word w : words){
		if(l.name.equals(w.name)){
			l.count++;
		}
	}
}

for(Word w : words){
	setW.add(w);
}

for(Word w : setW){
	for(Word w1 : words){
		if(w.name.equals(w1.name)){
			w.count++;
		}
	}
}
for(Word w : setW){
	for(Word w1 : lgm){
		if(w.name.equals(w1.name)){
			w.existLGM = true;
		}
	}
}

HSSFWorkbook doc = new HSSFWorkbook();
HSSFSheet sheet = doc.createSheet("ЛГМ за "+gLgm+" класс на Учебник "+gUch+" за 5 класс");

int rowCounter=0;
int cols = 0;
HSSFRow row = sheet.createRow(rowCounter++);
row.createCell(cols++).setCellValue("Слова из ЛГМ которые есть в учебнике");
row.createCell(cols++).setCellValue("Частота повторения в учебнике");
row.createCell(cols++).setCellValue("Слова из ЛГМ которых нет в учебнике");
row.createCell(cols++).setCellValue("Слова из Учебника которых нет в ЛГМ");
row.createCell(cols++).setCellValue("Частота повторения в учебнике");

for(Word l : lgm){
	cols=0;
	HSSFRow r = sheet.createRow(rowCounter++);
	if(l.count>0){
		row.createCell(cols++).setCellValue(l.name);
		row.createCell(cols++).setCellValue(l.count);
	}
	else{
		row.createCell(cols++).setCellValue(l.name);
	}
}
/*
rowCounter = 0;
for(Word w : setW){
	if(w.existLGM){
		rowCounter++;
		HSSFRow r = sheet.getRow(rowCounter);
		if(r==null){
			r = sheet.createRow(rowCounter);
		}
		r.createCell(4).setCellValue(w.name);
		r.createCell(5).setCellValue(w.count);
	}
}
*/
//write it as an excel attachment
ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
doc.write(outByteStream);
byte [] outArray = outByteStream.toByteArray();
response.setContentType("application/ms-excel");
response.setContentLength(outArray.length);
response.setHeader("Expires:", "0"); // eliminates browser caching
response.setHeader("Content-Disposition", "attachment; filename=testxls.xls");
OutputStream outStream = response.getOutputStream();
outStream.write(outArray);
outStream.flush();
%>