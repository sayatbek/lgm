<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">



<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <jsp:include page="libs.jsp" /> 

<script>
var waitingDialog = waitingDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			//$dialog.modal('hide');
		}
	};

})(jQuery);

</script>

<title>Загрузить ЛГМ</title>
</head>

<%String uploaded = request.getParameter("uploaded");
  String grade = request.getParameter("grade");%>

<body>
 	<jsp:include page="menus.jsp" />
	<div class="container">
	   <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h2>Загрузите ЛГМ для <span class="bg-danger"><%=grade %> класса</span> (формат .pdf)</h2>
        <h6>(Или выберите другой класс в меню навигационной панели)</h6>
		<form  class="form-group" method="POST" action="uploadLgm" enctype="multipart/form-data" >
           
            <label class="btn btn-primary" for="my-file-selector">
			    <input id="my-file-selector" name="fileMin" type="file" style="display:none;" onchange="$('#upload-file-info').html($(this).val());">
				   Выбрать файл
			</label>
			<span class='label label-info' id="upload-file-info"></span>
    		<input type="hidden" value="<%=request.getParameter("grade")%>" name="grade" /> 
            <input type="hidden" value="" name="destinationMin"/>
            </br>
            </br>
            <input class="btn btn-default" type="submit" value="Загрузить" name="uploadMin" id="uploadMin" onclick="waitingDialog.show('Идет загрузка...');setTimeout(function () {waitingDialog.hide();}, 2000);" />
        </form>
      </div>
      <div class="conatiner">
      	<%if(uploaded!=null&&!uploaded.isEmpty()){
      		if(uploaded.equals("encrypted")){
      			%>
      			<p class="bg-danger">Файл не загружен так как он "зашифрованный", 
      				файлы шифруются в момент их конвертации в формат .pdf</p>
      			<%
      		}
      		else{
      		%><p class="bg-danger">ЛГМ <%=uploaded %> для <%=grade %> класса загружен успешно!</p><%
      		} 
      	}%>
      </div>
</body>
</html>