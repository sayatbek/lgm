<%@page import="org.apache.pdfbox.text.PDFTextStripper"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.pdfbox.pdmodel.PDDocument"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.DBConnection"%>
<%@page import="java.sql.Connection"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="libs.jsp" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Запись в БД</title>
</head>
<body>

<%
	final Connection conn = (new DBConnection()).getConnection();
	PreparedStatement stmt = null;
	ResultSet rs = null;
	String sql = "";
	
	String fileName = request.getParameter("uploaded");
	String grade = request.getParameter("grade");
	String path = request.getParameter("path");
	
	
	
	try{
 		PDDocument document = null; 
 		File f=new File(path + File.separator + fileName);
 		document = PDDocument.load(f);
 		document.getClass();
 		
 		if( !document.isEncrypted() ){
 			System.out.println(fileName+" "+grade+" "+path);
 	 		
 			stmt = conn.prepareStatement("delete from files where grade=? and type=2", Statement.RETURN_GENERATED_KEYS);
 	    	stmt.setString(1, grade);
 			stmt.execute();
 			 
 			stmt = conn.prepareStatement("insert into files (`type`, `filename`, `grade`) values(?,?,?)", Statement.RETURN_GENERATED_KEYS);
 			stmt.setString(1, "2");
 			stmt.setString(2, fileName);
 			stmt.setString(3, grade);
			 stmt.execute();
			 	
			 stmt = conn.prepareStatement("delete from uchebnik where grade=?", Statement.RETURN_GENERATED_KEYS);
 	    	 stmt.setString(1, grade);
 			 stmt.execute();
 	    	
 			 PDFTextStripper Tstripper = new PDFTextStripper();
 		     
 			 putTextToHashSet(grade, Tstripper.getText(document), conn);
 		     
 		     Tstripper = null;
 		}else{
 			fileName = "encrypted";
 		}
 		document.close();
 		document = null;
 		f.delete();
 		
 		
 	}catch(Exception e){
 	    System.out.println(e.getMessage());
 	 	e.printStackTrace();
 	}	
	
	request.setCharacterEncoding("UTF-8");
	request.getRequestDispatcher("uploadUch.jsp?grade="+grade+"&uploaded="+fileName).forward(request, response);
%>

<%!
public static void putTextToHashSet(String grade, String st, Connection conn){
		String[] arr = st.toLowerCase().split("\\s+");
   	for(int i=0;i<arr.length;i++){
   		arr[i] = stripWord(arr[i]);
		   	if(arr[i].length()>1){
				String w = (arr[i].trim().toLowerCase());
		   		putTextToDataBase(grade, w, conn);
		   	}
	    }
   	arr = null;
  }
  
  public static void putTextToDataBase(String grade, String word, Connection conn){
  		try{
  	    	String sql = "INSERT INTO uchebnik (`word`, `grade`) VALUES (?, ?)";
  	    	PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
  			stmt.setString(1, word);
  			stmt.setString(2, grade);
      		stmt.execute();
      	}catch(Exception e){
  			System.out.println(e.getMessage());
  		}
  }
  
  public static String stripWord(String word){
  	word = word.replaceAll("i", "і");
		word = word.replaceAll("ї", "і");
		word = word.replaceAll("a", "а");
		word = word.replaceAll("b", "б");
		word = word.replaceAll("є", "қ");
		word = word.replaceAll("ііі", "ш");
		word = word.replaceAll("¥", "ұ");
		word = word.replaceAll("ӛ", "ө");
		word = word.replaceAll("ё", "е");
		
		word = word.replaceAll("(?!-)\\p{Punct}|\\p{IsDigit}|\\^-|x|v|l|«|»||ї|\\s|…|©|№|[a-zA-Z]|—|¹|”|“|•", "");
  	try{
	 		for(int j=0;j<5;j++){
		   		if(word.length()>1){
		   			if(word.charAt(word.length()-1) == '-' || word.charAt(0) == '–'){word = word.substring(0, word.length()-1);}
		   			if(word.charAt(0) == '-' || word.charAt(0) == '–'){ word = word.substring(1, word.length());}
		   		}
	    	}
  	}catch(Exception e){}
  	
  	return word;
  }
%>

</body>
</html>