<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.jsp">Главная</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	 <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Загрузить ЛГМ <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="uploadLgm.jsp?grade=1">1 класс</a></li>
                  <li><a href="uploadLgm.jsp?grade=2">2 класс</a></li>
                  <li><a href="uploadLgm.jsp?grade=3">3 класс</a></li>
                  <li><a href="uploadLgm.jsp?grade=4">4 класс</a></li>
                  <li><a href="uploadLgm.jsp?grade=5">5 класс</a></li>
                  <li><a href="uploadLgm.jsp?grade=6">6 класс</a></li>
                  <li><a href="uploadLgm.jsp?grade=7">7 класс</a></li>
                  <li><a href="uploadLgm.jsp?grade=8">8 класс</a></li>
                  <li><a href="uploadLgm.jsp?grade=9">9 класс</a></li>
                  <li><a href="uploadLgm.jsp?grade=10">10 класс</a></li>
                  <li><a href="uploadLgm.jsp?grade=11">11 класс</a></li>
                </ul>
              </li>
          </ul>
          <ul class="nav navbar-nav">
          	 <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Загрузить Учебное пособие <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="uploadUch.jsp?grade=1">1 класс</a></li>
                  <li><a href="uploadUch.jsp?grade=2">2 класс</a></li>
                  <li><a href="uploadUch.jsp?grade=3">3 класс</a></li>
                  <li><a href="uploadUch.jsp?grade=4">4 класс</a></li>
                  <li><a href="uploadUch.jsp?grade=5">5 класс</a></li>
                  <li><a href="uploadUch.jsp?grade=6">6 класс</a></li>
                  <li><a href="uploadUch.jsp?grade=7">7 класс</a></li>
                  <li><a href="uploadUch.jsp?grade=8">8 класс</a></li>
                  <li><a href="uploadUch.jsp?grade=9">9 класс</a></li>
                  <li><a href="uploadUch.jsp?grade=10">10 класс</a></li>
                  <li><a href="uploadUch.jsp?grade=11">11 класс</a></li>
                </ul>
              </li>
          </ul>
          
          <ul class="nav navbar-nav">
          	 <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Анализ <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="analyze.jsp">ЛГМ vs Учебное пособие</a></li>
                  <li><a href="analyzeUchebnik.jsp">Анализировать Учебное пособие</a></li>
                </ul>
              </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>