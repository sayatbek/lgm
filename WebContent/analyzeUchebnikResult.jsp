<%@page import="com.Stemmer"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="com.MyQuickSort"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.TreeSet"%>
<%@page import="com.Word"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.catalina.tribes.group.interceptors.TwoPhaseCommitInterceptor.MapEntry"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.DBConnection"%>
<%@page import="java.sql.Connection"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="libs.jsp" />
<title>Анализ Учебного пособия</title>
</head>
<% 
	DBConnection db = new DBConnection();
	final Connection conn = db.getConnection();
	PreparedStatement stmt= null, stmt1 = null;
	ResultSet rs = null, rs1 = null;
	String sql = "";
	
	String gUch = request.getParameter("gUch");
	
	//ArrayList<Word> lgm = new ArrayList<Word>();
	
	ArrayList<Word> wordsNew = new ArrayList<Word>();
	Set<Word> wordsSet = new TreeSet<Word>();
	//TreeSet<Word> dict = new TreeSet<Word>();
	Stemmer stemmer = new Stemmer(db);
	
	
	 sql = "select word from uchebnik where grade=? order by `word` ASC";
     stmt = conn.prepareStatement(sql);
     stmt.setString(1, gUch);
     rs = stmt.executeQuery();
     while(rs.next()){
     	 //String w = stemmer.stemWord(rs.getString(1));
    	 String w = rs.getString(1);
     	 if(w != null){
     	 	wordsNew.add(new Word(w));
    	 }
    	 else{
    		 wordsNew.add(new Word(rs.getString(1)));
    	 }
     }
     
     
     sql = "select distinct word from uchebnik where grade=? order by `word` ASC";
     stmt = conn.prepareStatement(sql);
     stmt.setString(1, gUch);
     rs = stmt.executeQuery();
     while(rs.next()){
    	 //String w = stemmer.stemWord(rs.getString(1));
    	 String w = rs.getString(1);
    	 if(w != null){
      		wordsSet.add((new Word(w)).trimZh());
     	 }
     	 else{
     		wordsSet.add((new Word(rs.getString(1)).trimZh()));
     	 }
     }
     
    
     for(Word l : wordsSet){
    	for(Word w : wordsNew){
      		if(l.isSimilar(w, db)){
      			l.count++;
      		}
      	}
     }
%>
<body>
<jsp:include page="menus.jsp" />

<div class="container">
  	<div class="row">
	  <div class="col-sm-4">
		  <h2>Сравнение</h2>
		  <p>Слова из ЛГМ и их частота повторения в учебнике</p>
		  
		  	<form  class="form-group" method="POST" action="excel" enctype="multipart/form-data" >
		        <input type="hidden" value="<%=gUch %>" name="gUch"/>
		        <input class="btn btn-success" type="submit" value="Выгрузить в эксель" name="uploadMin" id="uploadMin" />
		    </form>
		</div>
		<div class="col-sm-6">
				
		</div>
	</div>
	<div class="row">
   	</div>
	<div class="row">
	 
	 <div class="col-sm-2">
	  <table class="table">
	    <thead>
	      <tr>
	        <th>Уникальные слова из Учебника</th>
	        <th>Частота повторения в учебнике</th>
	      </tr>
	    </thead>
	    <tbody>
	   		  <%
	   		  int allnUch = 0;
	   		  int allnUchTotal = 0;
	   		  for(Word w : wordsSet){%>
			      <tr class="active">
			        <td><div class="cell bg-info"><%=w.name %></div></td>
			        <td><div class="cell bg-info"><%=w.count %></div></td>
			      </tr>
			      <% 
			      allnUch++;
			      allnUchTotal += w.count;
			  }%>
		      <tr class="active">
		        <td><div class="cell bg-warning">Всего: <%=allnUch%></div></td>
		        <td><div class="cell bg-warning">Всего: <%=allnUchTotal%></div></td>
		      </tr>
	   	</tbody>
	  </table>
	 </div>
	</div>
</div>
</body>
</html>